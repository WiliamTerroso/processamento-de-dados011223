#!/bin/bash

# Verifica se o arquivo de IP foi fornecido como argumento
if [ $# -eq 0 ]; then
    echo "Uso: $0 arquivo_de_ip"
    exit 1
fi

# Ordena o arquivo pela segunda coluna (assumindo que os IPs estão no formato xxx.xxx.xxx.xxx)
sort -t '.' -k2,2n $1
