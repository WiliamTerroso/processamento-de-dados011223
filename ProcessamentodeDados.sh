#!/bin/bash


# Passo 1: Criar um novo arquivo contendo apenas as 1000 primeiras linhas e as 1000 ultimes linhas do access.log.

head -1000 access.log > access_1000_lines.log
tail -1000 access.log >> access_1000_lines.log

# Passo 2: Remover o arquivo access.log

rm access.log

# Passo 3: Listar todos os IPs (sem repetição) que aparecem neste arquivo.

ips=$(cat access_1000_lines.log | awk '{print $4}' | sort | uniq)

# Passo 4: Fazer um ping (com uma repetição apenas) para cada um desses IPs.

for ip in ${ips}; do
  ping -c 1 $ip
done
