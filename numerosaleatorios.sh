#!/bin/bash

# Gera 100 números aleatórios e armazena em um array
for ((i=0; i<100; i++)); do
    numeros[$i]=$((RANDOM % 100))  # Gera números aleatórios de 0 a 100 
done

# Ordena o array de forma decrescente
sorted=($(printf "%s\n" "${numeros[@]}" | sort -nr))

# Imprime os números ordenados
echo "Números aleatórios do maior para o menor:"
for num in "${sorted[@]}"; do
    echo $num
done

